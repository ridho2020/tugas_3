-- insert data to table employee
INSERT INTO employee(`nama`, `atasan_id`, `company_id`) VALUES
	('Pak Budi', null, '1'),   
	('Pak Tono', '1', '1'),   
	('Pak Totok', '1', '1'),   
	('Bu Sinta', '2', '1'),   
	('Bu Novi', '3', '1'),   
	('Andre', '4', '1'),   
	('Dono', '4', '1'),
	('Ismair', '5', '1'),
	('Anto', '5', '1');

-- insert data to table company
INSERT INTO company(`nama`, `alamat`) VALUES
	('PT JAVAN', 'Sleman'),   
	('PT Dicoding', 'Bandung');


-- CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO
SELECT * FROM employee where atasan_id is null;
-- Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa
SELECT * FROM employee where atasan_id >= 4;
-- Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur
SELECT * FROM employee where atasan_id = 1;
-- Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager
SELECT * FROM employee where atasan_id = 4 or atasan_id = 5;
-- Bawahan pak budi berjumlah 8 orang
SELECT count(*) as Karyawan_Pak_Budi FROM employee where employee.nama != "Pak Tono";
-- Bawahan Bu Sinta berjumlah 2 orang
SELECT  count(*) as Bawahan_Bu_Shinta FROM employee WHERE employee.nama != "Bu Sinta" and employee.atasan_id = '4';
